//2016-5-20
//master-slave mode sync


const fs = require('fs');
const fse = require('fs-extra')
const async = require('async');
const crypto = require('crypto');


const FROM = 1;
const TO = 2;


var getSHA1 = (filepath, cb1) => {
    // console.log('getSHA1', filepath); ///////////////
    fs.stat(filepath, (err, stats) => {
        if (err) {
            // console.log("aaa"); ////////////
            return cb1(err, undefined);
        }
        if (stats.isDirectory()) {
            // console.log("bbbbb"); ////////////
            return cb1({
                err: 'can not gen sha1 from a directory',
                path: filepath
            }, null);
        }
        var hash = crypto.createHash('sha1');
        hash.setEncoding('hex');

        // console.log("cccc"); ////////////
        //read file
        var fp = fs.createReadStream(filepath);
        fp.on('end', () => {
            hash.end();
            console.log(hash.read()); ////////////
            // console.log("dddddd"); ////////////
            return cb1(null, hash.read());
        });

        fp.pipe(hash);
    });
};

var copyNow = (masterPath, slavePath, copyDir, cb2) => {
    fs.stat(masterPath, (err, stats) => {
        if (err) {
            return cb(err);
        }

        if (stats.isDirectory()) {
            return cb2({
                err: 'can not copy directory now',
                path: masterPath
            });
        }
        switch (copyDir) {
            case TO:
                console.log(masterPath, ' => ', slavePath);
                fse.copy(masterPath, slavePath, (err) => {
                    if (err) {
                        return console.error(err);
                    }
                    cb2(null);
                    //done
                });
                break;

            case FROM:
                // console.log(masterPath, ' <= ', slavePath);
                // fse.copy(slavePath, masterPath, (err) => {
                //     if (err) {
                //         return console.error(err);
                //     }
                // })
                cb2(null);
                break;

            default:
                // console.log(masterPath, ' == ', slavePath);
                cb2(null);
                break;
        }

    })
};


var syncFile = (masterPath, slavePath, cb2) => {
    // console.log('syncFile',
    // masterPath, slavePath); /////////////////////////
    async.map([masterPath, slavePath], fs.stat, (err, results) => {
        if (err) {
            // console.error(err); ////////////
        }

        var stats1 = results[0];
        var stats2 = results[1];

        // console.log('11111'); ////////////////////

        if (typeof stats1 == 'undefined') {
            // console.log('2222'); ////////////////////
            //do nothing
            return;
        }
        if (stats1 && typeof stats2 == 'undefined') {
            // console.log('3333'); ////////////////////
            return copyNow(masterPath, slavePath, TO, cb2);
        }

        var copyDir;
        async.map([masterPath, slavePath], getSHA1, (err, results2) => {
            // console.log('5555'); ////////////////////
            // console.log(results2); ////////////////
            if (results2[0] == results2[1]) {
                //do nothing
                // console.log('6666'); ////////////////////
                return;
            }

            if (stats1.mtime > stats2.mtime) {
                // console.log('7777'); ////////////////////
                return copyNow(masterPath, slavePath, TO, cb2);
            }

            if (stats1.mtime < stats2.mtime) {
                // console.log('9999'); ////////////////////
                return copyNow(masterPath, slavePath, FROM, cb2);
            }

        });
    });
};



module.exports.syncFile = syncFile;